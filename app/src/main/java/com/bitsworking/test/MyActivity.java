package com.bitsworking.test;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;


public class MyActivity extends Activity {
    private final static String TAG = "MyActivity";

    static class Repo {
        public final int id;
        public final String full_name;

        Repo(int id, String full_name) {
            this.id = id;
            this.full_name = full_name;
        }
    }

    public interface GitHubListRepoService {
        @GET("/users/{user}/repos")
        List<Repo> listRepos(@Path("user") String user);
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        if (!isOnline()) {
            showNotOnlineDialog();
            return;
        }
    }

    private void showNotOnlineDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("No network connection :(")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                        finish();
                    }
                });

        // Create the AlertDialog object and return it
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        TextView repoList;
        Handler mHandler = new Handler();

        public PlaceholderFragment() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_my, container, false);

            Button button = (Button) rootView.findViewById(R.id.button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getActivity(), "Button Clicked", Toast.LENGTH_LONG).show();
                }
            });

            repoList = (TextView) rootView.findViewById(R.id.tv_repoList);
            updateRepos();

            return rootView;
        }

        private void updateRepos() {
            new Thread(new Runnable() {
                public void run() {
                    RestAdapter restAdapter = new RestAdapter.Builder()
                            .setEndpoint("https://api.github.com")
                            .build();

                    GitHubListRepoService service = restAdapter.create(GitHubListRepoService.class);
                    final List<Repo> repos = service.listRepos("octocat");

                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            repoList.setText("");

                            for (Repo r : repos) {
                                Log.v(TAG, "repo: " + r.full_name);
                                repoList.setText(repoList.getText() + "\n" + r.full_name);
                            }
                        }
                    });
                }
            }).start();
        }

        @Override
        public void onPause() {
            super.onPause();
            // Commit any persistent changes
        }

    }
}
